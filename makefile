#############
# Asetukset #
#############

# C-kompiloija ja sille annettavat flagit
CC = gcc
CFLAGS = -g -Wall -Wextra -std=c99

# Handle viope
ifdef VIOPE
CFLAGS += -D VIOPE
endif

# Add coverage
ifdef COVERAGE
CFLAGS += -fprofile-arcs -ftest-coverage
endif

# C++ kompiloija
CXX = g++

# C-Preprocessorille annettavat flagit
# Tässä määritellään GoogleTest kansiossa olevat headerit ns. systeemiheadereiksi,
# jolloin ne eivät nosta varoituksia kompiloitaessa.
# Tähän ei tarvitse koskea.
CPPFLAGS += -isystem $(GTEST_DIR)/include
# C++ kompiloijalle annettavat flagit, suurimmaksi osaksi samat kuin CFLAGS
# Tähänkään ei oikeastaan tarvitse koskea.
CXXFLAGS += -g -Wall -Wextra -pthread

# Kansio, josta googletest lähdekoodi löytyy relatiivisesti tähän makefileen.
# Huom: Googletest jakautuu GoogleTest ja GoogleMock projekteihin,
# Tässä tarvitaan se "alempi" GoogleTest kansio.
GTEST_DIR = ./googletest/googletest

# Kolmella alla olevalla muuttujalla hallitaan projektin kansiorakennetta:
# 
# SRC_DIR:   Kansio, jossa lähdekoodi sijaitsee
# BUILD_DIR: Kansio, johon kompiloidut objektit ja valmis ohjelma tallennetaan.
# TESTS_DIR: Kansio, jossa kaikki testit sijaitsevat ja johon
# 			 kompiloidut testit tallenntetaan.
SRC_DIR = ./src
BUILD_DIR = ./build
TESTS_DIR = ./tests

# Kaikki testit, jotka GoogleTest tuottaa,
# Testit ovat käytännössä suoritettavia ohjelmia, jotka ajavat testit.
# Näin pienessä projektissa ei ole tarvetta erotella testejäohjelmia, joten
# tässä riittää mainita vain yksi nimi.
# Huom: $(TESTS_DIR) määritellyssä kansiossa tulee olla samannimiset
# lähdekooditiedostot, esim. "testit.cc"
# .cc merkkaa, että ne ovat oikeasti C++ lähdekooditiedostoja.
TESTS = tests

# Valmiin ohjelman tiedostonimi
EXECUTABLE_NAME = pullonpalautus

# Kompiloitavat objektit.
# MAIN_OBJECT merkkaa objektin tai moduulin, jossa main() sijaitsee
# OBJECTS taas merkkaa objektit, jotka linkataan myöhemmin.
# OBJECTS linkataan myös testien kanssa.
# 
# Syy miksi main.o ei linkata testien kanssa on, että GoogleTest rakentaa
# oman main() funktion, joka kutsuu kaikki testit, oma main() olisi siis tiellä.
MAIN_OBJECT = main.o
OBJECTS = tietorakenne.o tiedosto.o funktio.o

# GoogleTest headerit ja lähdekooditiedostot
# Näihin ei tarvitse koskea ellei tiedä mitä tekee.
GTEST_HEADERS = $(GTEST_DIR)/include/gtest/*.h \
                $(GTEST_DIR)/include/gtest/internal/*.h
GTEST_SRCS_ = $(GTEST_DIR)/src/*.cc $(GTEST_DIR)/src/*.h $(GTEST_HEADERS)

# Makefile taikaa
# Virtualisoi lähdekoodipolut.
# Näin src/ etuliitettä ei tarvitse kirjoittaa erikseen .c ja .h tiedostoihin,
# vaan make tietää etsiä puuttuvia tiedostoja näistä kansioista.
# https://www.gnu.org/software/make/manual/html_node/Selective-Search.html#Selective-Search
vpath %.c src
vpath %.h src

# Sama, mutta testijutuille
vpath %.cc tests
vpath %.a tests

# Objekteja voi olla sekä buildissa, että testeissä
vpath %.o build:tests

#########
# Rules #
#########

# All tarkoittaa yksinkertaisesti, että kompiloidaan sekä ohjelma, että testit
all : dirs $(EXECUTABLE_NAME) $(TESTS)

# Itse ohjelman kompilointi
# Kun prerequisitena on .o päätteisiä asioita,
# ne matchaavat alempaan %.o sääntöön, mikä kompiloi ne.
$(EXECUTABLE_NAME) : $(MAIN_OBJECT) $(OBJECTS)
	$(CC) $(CFLAGS) $(addprefix $(BUILD_DIR)/, $(MAIN_OBJECT) $(OBJECTS)) -o $(BUILD_DIR)/$@
	cp $(SRC_DIR)/tuotetiedosto.txt $(BUILD_DIR)/tuotetiedosto.txt

# Rule, joka varmistaa, että vaadittavat kansiot ovat olemassa
dirs:
	mkdir -p $(SRC_DIR) $(BUILD_DIR) $(TESTS_DIR)

# Kaikki objektit kompiloidaan samalla tavalla, joten riittää yksi rule.
# 
# Suora esimerkki Make:n docseista:
# https://www.gnu.org/software/make/manual/html_node/Pattern-Examples.html#Pattern-Examples
# 
# $@ = Rulen nimi: tulee olemaan esim. tietorakenne.o
# $< = Ensimmäinen prerequisite: tulee olemaan esim. tietorakenne.c
%.o : %.c
	$(CC) $(CFLAGS) -c -o $(BUILD_DIR)/$@ $<

# GoogleTest testit voidaan kompiloida ja linkata kahdella eri tavalla:
# 1. Käyttää gtest_main
# 2. Käyttää gtest
# Ainoana erona on, että gtest_main tuo oman main() funktion.
# (siksi ei linkata main.o kanssa)
tests: tests.cc $(OBJECTS) gtest_main.a
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -lpthread $(TESTS_DIR)/tests.cc \
		$(addprefix $(BUILD_DIR)/,$(OBJECTS)) -o $(TESTS_DIR)/$@ $(TESTS_DIR)/gtest_main.a

clean :
	rm -vf $(BUILD_DIR)/* $(TESTS_DIR)/*.o $(TESTS_DIR)/*.a $(addprefix $(TESTS_DIR)/,$(TESTS))


#############################
# GoogleTest sisäiset rulet #
#############################
gtest-all.o : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) $(CXXFLAGS) -c \
            $(GTEST_DIR)/src/gtest-all.cc -o $(TESTS_DIR)/$@

gtest_main.o : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) $(CXXFLAGS) -c \
            $(GTEST_DIR)/src/gtest_main.cc -o $(TESTS_DIR)/$@

gtest.a : gtest-all.o
	$(AR) $(ARFLAGS) -o $(TESTS_DIR)/$@ $(addprefix $(TESTS_DIR)/,$^)

gtest_main.a : gtest-all.o gtest_main.o
	$(AR) $(ARFLAGS) -o $(TESTS_DIR)/$@ $(addprefix $(TESTS_DIR)/,$^)
