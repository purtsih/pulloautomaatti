/*
 * CT60A0210 Käytännön ohjelmoinnin harjoitustyö
 * Tekijä: Henna Pekkala
 * Opiskelijanumero: 0456204
 * Päivämäärä: 29.4.2016
 * Yhteistyö ja lähteet, nimi ja yhteistyön muoto:
 * Ryhmä:
 *  -Henna Pekkala
 *  -Jani Purhonen
 *  -Karoliina Varso
 * Lähteet:
 *  -Luennot
 *  -C-opas
 *  -fflush: http://man7.org/linux/man-pages/man3/fflush.3.html
 */

#include "funktio.h"
#include "tiedosto.h"
#include "tietorakenne.h"

#include <stdlib.h>
#include <string.h>

FILE* avaa_tuotetiedosto(char* filename) {
    if (filename) {
        FILE* opened = fopen(filename, "r");
        if (opened) {
            return opened;
        } else {
            perror("Tuotetiedoston avaaminen epäonnistui!");
            exit(1);
        }
    }

    fprintf(stderr, "Tuotetietojen tiedostonimi oli tyhjä!\n");
    exit(1);
}

/**
 * Lue tuotetiedot listaan.
 * @param lista     Listaosoitin
 * @param tiedosto  Tuotetiedosto
 */
void lue_tuotetiedot_listaan(Tuote** lista, FILE* tiedosto) {
    char rivi[100];
    char nimi[64];
    float koko;
    float pantti;
    int laskuri = 0;

    while (fgets(rivi, 100, tiedosto) != NULL) {
        if (sscanf(rivi, "%s %f %f", nimi, &koko, &pantti) == 3) {
            /* Tarkastetaan, että oikeanlainen rivi tiedostossa. 
             * Lisätään tuote listan perään */
            lisaa_tuote(lista, nimi, koko, pantti);
            laskuri++;
        } else {
            printf("\n=== Virheellinen tuote tuotetiedoissa ===\nRivi \"%s\" hypätään yli.\n", rivi);
        }
    }

    if (laskuri == 0) {
        perror("\nTuotetiedosto on tyhjä.\n");
        exit(2);
    }

    fclose(tiedosto);
}

/**
 * Avaa tilapäislogin.
 * @return  Tilapäislogitiedosto
 */
FILE* avaa_tilapaisloki(void) {
    FILE* tiedosto;

    if ((tiedosto = fopen("tilapaistiedosto.txt","w")) == NULL) {
        perror("\nTilapäistiedoston avaaminen epäonnistui.\n");
        exit(3);
    }

    fprintf(tiedosto, "%s\n\n", "Tilapäinen lokitiedosto");
    return tiedosto;
}

/**
 * Avaa päälogin.
 * @return  Päälogitiedosto
 */
FILE* avaa_paaloki(void) {
    FILE* tiedosto;

    if ((tiedosto = fopen("lokitiedosto.txt","a")) == NULL) {
        perror("\nLokitiedoston avaaminen epäonnistui.\n");
        exit(4);
    }
    
    return tiedosto;
}

/**
 * Kirjoittaa tilapäislokiin palautuksen.
 * Historiallisista syistä sisältää hieman Viopespeciffiä koodia.
 * @param tiedosto Lokitiedosto
 * @param tyyppi   Palautuksen tyyppi
 * @param koko     Palautuksen koko
 * @param pantti   Palautuksen pantti
 */
void kirjoita_tilapaisloki(FILE* tiedosto, char* tyyppi, float koko, float pantti) {
    #if VIOPE
        fprintf(tiedosto, "%s-%.2fl:%.2f€.\n", tyyppi, koko, pantti);
        fflush(tiedosto);
    #else
        char aika[TIMESTAMP_LENGTH];
        timestamp(aika);

        fprintf(tiedosto, "%s:%s-%.2fl:%.2f€.\n", aika, tyyppi, koko, pantti);
        fflush(tiedosto);
    #endif
}

/**
 * Kirjoittaa päälogiin.
 * Sisältää myös Viopekoodia.
 * @param tiedosto    Päälogitiedosto.
 * @param pullo_maara Palautettujen pullojen määrä.
 * @param pantti      Palautettujen pullojen kokonaispantti.
 */
void kirjoita_paaloki(FILE* tiedosto, int pullo_maara, float pantti) {
    #if VIOPE
        fprintf(tiedosto, "Palautukset %d kpl. Pantit %.2f€.\n", pullo_maara, pantti);
        fflush(tiedosto);
    #else
        char aika[TIMESTAMP_LENGTH];
        timestamp(aika);

        fprintf(tiedosto, "%s - Palautukset %d kpl. Pantit %.2f€.\n", aika, pullo_maara, pantti);
        fflush(tiedosto);
    #endif
}

/**
 * Sulkee logitiedostot.
 * @param tiedosto Logitiedosto
 */
void sulje_loki(FILE** tiedosto) {
    if (*tiedosto == NULL) {
        return;
    }

    fclose(*tiedosto);
    *tiedosto = NULL;
}
