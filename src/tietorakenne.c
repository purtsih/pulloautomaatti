/*
* CT60A0210 Käytännön ohjelmointi -kurssin ohjelmien otsikkotiedot.
* Tekijä: Karoliina Varso
* Opiskelijanumero: 0456097
* Päivämäärä: 29.4.2016
* Yhteistyö ja lähteet, nimi ja yhteistyön muoto:
* Ryhmä:
*      Jani Purhonen
*      Henna Pekkala
*      Karoliina Varso
*/

#include "tietorakenne.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Lisää tuote listaan
 * @param lista Osoitin listaan
 * @param t     Tyyppi (Nimi)
 * @param k     Koko
 * @param p     Pantti
 */
void lisaa_tuote(Tuote** lista, char* t, float k, float p) {
    Tuote *nykyinen;
    Tuote *uusi;

    if ((uusi = malloc(sizeof(Tuote))) == NULL) { /* Varataan muistia */
        perror("Muistin varaus epäonnistui.\n");
        exit(1);
    }

    if ((uusi->tyyppi = malloc((strlen(t) + 1) * sizeof(char))) == NULL) {
        perror("Muistin varaus epäonnistui.\n");
        exit(1);
    }

    strcpy(uusi->tyyppi, t); /* Uuden pullon arvojen asettaminen */
    uusi->koko   = k;
    uusi->pantti = p;
    uusi->pSeuraava = NULL;

    if (*lista == NULL) {
        *lista = uusi; /* Alustaa listan */
    } else {
        nykyinen = *lista; /* Kelaa listaa kunnes seuraavaa pulloa ei ole */
        while (nykyinen->pSeuraava != NULL) {
            nykyinen = nykyinen->pSeuraava;
        }
        nykyinen->pSeuraava = uusi;
    }
}

/**
 * Lisää palautuslistaan.
 * @param lista  Osoitin listaan
 * @param numero Indeksi tuotelistassa
 */
void lisaa_palautus(Palautus** lista, int numero) {
    Palautus* nykyinen;
    Palautus* uusi;

    if ((uusi = malloc(sizeof(Palautus))) == NULL) {  /* Varataan muistia */
        perror("Muistin varaus epäonnistui.\n");
        exit(1);
    }
    uusi->index = numero; /* Uuden palautuksen arvon asettaminen */
    uusi->pSeuraava = NULL;
    if (*lista == NULL) {
        *lista = uusi;    /* Palautuksen lisääminen listaan */
    } else {
        nykyinen = *lista;
        while(nykyinen->pSeuraava != NULL) {
            nykyinen = nykyinen->pSeuraava;
        }
        nykyinen->pSeuraava = uusi;
    }
}

/**
 * Etsii tuotteen listasta indeksin avulla.
 * @param  lista   Lista
 * @param  indeksi Indeksi
 * @return         Löydetty tuote tai NULL
 */
Tuote* etsi_tuote(Tuote* lista, int indeksi) {
    Tuote* nykyinen = lista;
    int i = 1;

    if (lista == NULL) {
        return NULL;
    }

    if (indeksi == 1) { /* Ensimmäinen alkio */
        return lista;
    } else {
        while (i < indeksi) { /* Menee eteenpäin, kunnes haluttu alkio löydetty */
            nykyinen = nykyinen->pSeuraava;
            i++;
        }
        return nykyinen;
    }

    return NULL;
}

/**
 * Laske tuotelistan pituus
 * @param  lista Lista
 * @return       Tuotelistan pituus
 */
int tuote_pituus(Tuote* lista) {
    Tuote* nykyinen = lista;
    int pituus = 0;

    if (lista != NULL) {
        while (nykyinen != NULL) {
            pituus++;
            nykyinen = nykyinen->pSeuraava;
        }
    }

    return pituus;
}

/**
 * Laskee palautuslistan pituuden.
 * Huom: EI sama kuin palautettujen pullojen määrä.
 * @param  lista Lista
 * @return       Palautuslistan pituus.
 */
int palautus_pituus(Palautus* lista) {
    Palautus* nykyinen = lista;
    int pituus = 0;

    if (lista != NULL) {
        while (nykyinen != NULL) {
            pituus++;
            nykyinen = nykyinen->pSeuraava;
        }
    }

    return pituus;
}


/**
 * Vapauttaa tuotelistan.
 * @param lista Lista
 */
void tyhjenna_tuotelista(Tuote** lista) {
    Tuote* nykyinen = *lista;
    while (nykyinen != NULL) {
        *lista = nykyinen->pSeuraava;
        free(nykyinen->tyyppi);
        free(nykyinen);
        nykyinen = *lista;
    }

    *lista = NULL;
}

/**
 * Vapauttaa palautuslistalle varatun muistin.
 * @param lista Lista
 */
void tyhjenna_palautuslista(Palautus** lista) {
    Palautus* nykyinen = *lista;

    while (nykyinen != NULL) {
        *lista = nykyinen->pSeuraava;
        free(nykyinen);
        nykyinen = *lista;
    }

    *lista = NULL;
}
