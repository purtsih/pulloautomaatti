#include "gtest/gtest.h"

// Disable C++ function name mangling
#ifdef __cplusplus
extern "C" {
#endif
    // Includee kaikki tarvittavat headerit tässä.
    // Tiedostopathit relatiivisesti tähän tiedostoon
    #include "../src/funktio.h"
    #include "../src/tietorakenne.h"
    #include "../src/tiedosto.h"
#ifdef __cplusplus
}
#endif

// Ignoree -Wwrite-strings varoitukset
#pragma GCC diagnostic ignored "-Wwrite-strings"

/**
 * Testaa listan luominen
 */
TEST(LinkedList, AddOne) {
    Tuote* lista = NULL;
    lisaa_tuote(&lista, "Testipullo", 0.33f, 0.25f);

    // Testaa voidaanko listaan lisätä yksi tuote
    ASSERT_STREQ(lista->tyyppi, "Testipullo");
    ASSERT_EQ(lista->pantti, 0.25f);
    ASSERT_EQ(lista->koko, 0.33f);

    tyhjenna_tuotelista(&lista);
}

/**
 * Testaa jo luotuun listaan lisääminen
 */
TEST(LinkedList, AddSecond) {
    Tuote* lista = NULL;
    lisaa_tuote(&lista, "Ensimmäinen", 0.33f, 0.25f);
    lisaa_tuote(&lista, "Toinen", 0.50f, 0.40f);

    // Listan toisena tuotteena pitäisi nyt olla pullo "Toinen"
    ASSERT_STREQ(lista->pSeuraava->tyyppi, "Toinen");
    ASSERT_EQ(lista->pSeuraava->koko, 0.50f);
    ASSERT_EQ(lista->pSeuraava->pantti, 0.40f);

    tyhjenna_tuotelista(&lista);
}

/**
 * Testaa tuotelistan pituuden laskeminen
 */
TEST(LinkedList, TuoteLength) {
    Tuote* lista = NULL;

    ASSERT_EQ(tuote_pituus(lista), 0);

    lisaa_tuote(&lista, "Asd", 0.10f, 0.10f);
    ASSERT_EQ(tuote_pituus(lista), 1);

    lisaa_tuote(&lista, "Asd", 0.10f, 0.10f);
    ASSERT_EQ(tuote_pituus(lista), 2);

    tyhjenna_tuotelista(&lista);
}

TEST(LinkedList, TuoteLengthBig) {
    Tuote* lista = NULL;
    int n = 1024 * 10; /* Lisättävien tuotteiden määrä */

    for (int i = 0; i < n; ++i)
    {
        lisaa_tuote(&lista, "Testi", 1.00f, 1.00f);
    }

    ASSERT_EQ(tuote_pituus(lista), n);

    tyhjenna_tuotelista(&lista);
}

/**
 * Testaa palautuslistan pituuden laskeminen
 */
TEST(LinkedList, PalautusLength) {
    Palautus* lista = NULL;

    ASSERT_EQ(palautus_pituus(lista), 0);

    lisaa_palautus(&lista, 1);
    ASSERT_EQ(palautus_pituus(lista), 1);

    lisaa_palautus(&lista, 1);
    ASSERT_EQ(palautus_pituus(lista), 2);

    // Cleanup
    tyhjenna_palautuslista(&lista);
}

TEST(LinkedList, PalautusLengthBig) {
    Palautus* lista = NULL;

    int n = 1024 * 10;

    for (int i = 0; i < n; ++i)
    {
        lisaa_palautus(&lista, 1);
    }

    ASSERT_EQ(palautus_pituus(lista), n);

    tyhjenna_palautuslista(&lista);
}

/**
 * Testaa tuotteen löytäminen
 */
TEST(LinkedList, Find) {
    Tuote* lista = NULL;
    lisaa_tuote(&lista, "Pullo 1", 0.10f, 0.10f);
    lisaa_tuote(&lista, "Pullo 2", 0.10f, 0.10f);
    lisaa_tuote(&lista, "Pullo 3", 0.10f, 0.10f);
    lisaa_tuote(&lista, "Pullo 4", 0.10f, 0.10f);
    lisaa_tuote(&lista, "Pullo 5", 0.10f, 0.10f);

    Tuote* found = etsi_tuote(lista, 3); // Etsitään pullo 3

    ASSERT_STREQ(found->tyyppi, "Pullo 3");
}

/**
 * Testaa kokonaispantin laskeminen
 */
TEST(Palautukset, Kokonaispantti) {
    // Kasaa feikki tuotelista ja palautuslista
    Tuote*    tuotteet    = NULL;
    Palautus* palautukset = NULL;

    // Lisätään kolme tuotetta
    lisaa_tuote(&tuotteet, "One", 0.10f, 0.20f);
    lisaa_tuote(&tuotteet, "Two", 0.10f, 0.30f);
    lisaa_tuote(&tuotteet, "Three", 0.10f, 0.40f);

    // Palautetaan pulloja ja testataan joka palautuksen jälkeen kokonaispantti
    lisaa_palautus(&palautukset, 1);
    ASSERT_FLOAT_EQ(kokonaispantti(tuotteet, palautukset), 0.20f);

    lisaa_palautus(&palautukset, 2);
    ASSERT_FLOAT_EQ(kokonaispantti(tuotteet, palautukset), 0.50f);

    lisaa_palautus(&palautukset, 3);
    ASSERT_FLOAT_EQ(kokonaispantti(tuotteet, palautukset), 0.90f);

    lisaa_palautus(&palautukset, 1);
    ASSERT_FLOAT_EQ(kokonaispantti(tuotteet, palautukset), 1.10f);

    // Clean up
    tyhjenna_tuotelista(&tuotteet);
    tyhjenna_palautuslista(&palautukset);
}

/**
 * Testaa tuotetietojen lukeminen tiedostosta.
 */
TEST(IO, ReadProductsFromFile) {
    // Fake tuotelistan sisältö
    char products[] = "Testipullo 0.33 0.10\n"
                      "Testitölkki 0.50 0.20";

    FILE* fakefile = fmemopen(products, sizeof(products), "r");
    Tuote* lista   = NULL; // Lista mihin tuotteet luetaan

    lue_tuotetiedot_listaan(&lista, fakefile);

    // Testaa että kaikki tuotteet luettiin oikein
    ASSERT_STREQ(lista->tyyppi, "Testipullo");
    ASSERT_EQ(lista->koko, 0.33f);
    ASSERT_EQ(lista->pantti, 0.10f);

    ASSERT_STREQ(lista->pSeuraava->tyyppi, "Testitölkki");
    ASSERT_EQ(lista->pSeuraava->koko, 0.50f);
    ASSERT_EQ(lista->pSeuraava->pantti, 0.20f);

    // Cleanup
    tyhjenna_tuotelista(&lista);
    fclose(fakefile);
}
