# Pulloautomaatti

## Requirements
Only requirements are a C and C++ compiler (GCC or Clang), and GNU-compatible Make.

Tests depend on [GoogleTest](https://github.com/google/googletest/) which has its own (but minimal) requirements and should usually compile without any problems.

## Directory structure
| Directory | Description                       |
| --------- | --------------------------------- |
| `src/`    | Source code                       |
| `build/`  | Binaries and executables          |
| `tests/`  | Test sources and test executables |

## Getting started
Clone the repository and build with `make`. If you plan to build tests initialize git submodules to clone into [GoogleTest](https://github.com/google/googletest/).

```bash
git clone https://bitbucket.org/purtsih/pulloautomaatti.git

# Only if you plan to build tests
git submodule init
git submodule update

# Build
cd pullonpalautus
make all

# Run tests
./tests/tests
```

Build will be in placed in `build/` directory and test executable in `tests/`.

**Note**: You only need to use `git submodule init` once.

## Building
Build system uses `make`. The following rules are supported:

| Command               | Description                    |
| --------------------- | ------------------------------ |
| `make all`            | Make program and tests         |
| `make pullonpalautus` | Make program only              |
| `make tests`          | Make tests only                |
| `make clean`          | Clean build and test directory |

### Viope specific builds
For historical reasons there's a special flag for a build that strips timestamps from output and logfiles. To build this pass `VIOPE=1` after any make command. For example: `make all VIOPE=1`

### Code Coverage
To enable code coverage flags (`-fprofile-arcs` and `-ftest-coverage`), compile using `make pullonpalautus COVERAGE=1`. **NOTE:** You cannot build tests while coverage flags are enabled (because C++ is dumb).
